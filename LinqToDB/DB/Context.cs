﻿using LinqToDB.Data;
using LinqToDB.Model;

namespace LinqToDB.DB
{
    public class Context : DataConnection
    {
        public Context() : base("default") { }

        public ITable<animais_manejos> AnimaisManejos => GetTable<animais_manejos>();
        public ITable<animais> Animais => GetTable<animais>();
        public ITable<manejos> Manejos => GetTable<manejos>();
        public ITable<piquete_curral> PiqueteCurral => GetTable<piquete_curral>();
        public ITable<protocolo_sanitario> ProtocoloSanitario => GetTable<protocolo_sanitario>();
        public ITable<racas> Racas => GetTable<racas>();
        public ITable<rebanho> Rebanho => GetTable<rebanho>();
        public ITable<users> Users => GetTable<users>();
    }
}
