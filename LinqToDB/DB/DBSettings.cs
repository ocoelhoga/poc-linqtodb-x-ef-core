﻿using LinqToDB.Configuration;
using LinqToDB.Data;
using LinqToDB.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqToDB.DB
{
    public class DBSettings : ILinqToDBSettings
    {
        public IEnumerable<IDataProviderSettings> DataProviders => Enumerable.Empty<IDataProviderSettings>();

        public string DefaultConfiguration => "default";
        public string DefaultDataProvider => ProviderName.PostgreSQL95;

        public IEnumerable<IConnectionStringSettings> ConnectionStrings
        {
            get
            {
                yield return
                    new ConnectionStringSettings
                    {
                        Name = "default",
                        ProviderName = ProviderName.PostgreSQL95,
                        ConnectionString = @"User ID=postgres;Password=postgres;Server=127.0.0.1;Port=5432;Database=piratininga;Integrated Security=true;Pooling=true;"
                    };
            }
        }
    }
    public class ConnectionStringSettings : IConnectionStringSettings
    {
        public string ConnectionString { get; set; }
        public string Name { get; set; }
        public string ProviderName { get; set; }
        public bool IsGlobal => false;
    }

    
}
