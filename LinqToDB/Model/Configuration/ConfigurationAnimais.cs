﻿using LinqToDB.Data;
using LinqToDB.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqToDB.Model.Configuration
{
    public class ConfigurationAnimais : DataConnection
    {
        public ConfigurationAnimais()
        {
            var mappingSchema = new MappingSchema();
            var builder = mappingSchema.GetFluentMappingBuilder();

            builder.Entity<animais>()
                .HasTableName("animais")
                .HasSchemaName("public")
                .HasIdentity(x => x.id)
                .HasPrimaryKey(x => new { x.id , x.implantations_id})
                ;

            this.AddMappingSchema(mappingSchema);
        }

    }
}
