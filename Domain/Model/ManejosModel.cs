﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Model
{
	public class ManejosModel
	{
		public virtual long? id { get; set; }
		public double? dt_update { get; set; }
		public double? dt_register { get; set; }
		public int? status { get; set; }
		public long? implantations_id { get; set; }
		public long? users_id { get; set; }
		public long? workgroups_id { get; set; }
		public long? last_update_user_id { get; set; }
		public string sent { get; set; }
		public long? propriedade_id { get; set; }
		public double? dt_inicio { get; set; }
		public double? dt_termino { get; set; }
		public long? documento_id { get; set; }
		public long? lotes_manejo_id { get; set; }
		public long? piquete_curral_id { get; set; }
		public long? curral_manejo_id { get; set; }
		public int? tipo { get; set; }
		public int? animais_previsto { get; set; }
		public int? animais_realizado { get; set; }
		public long? retiro_confinamento_id { get; set; }
		public int? status_manejo { get; set; }
		public int? tipo_entrada { get; set; }
		public int? aparte_peso { get; set; }
		public int? aparte_era { get; set; }
		public int? aparte_raca { get; set; }
		public double? tempo_trabalho { get; set; }
		public int? usar_sisbov { get; set; }
		public int? usar_ncf { get; set; }
		public int? usar_chip { get; set; }
		public long? device_id { get; set; }
		public int? usar_bnd { get; set; }
		public int? usar_peso { get; set; }
		public string sisbov_inicio { get; set; }
		public string sisbov_final { get; set; }
		public int? max_digito_chip { get; set; }
		public int? travar_retiro { get; set; }
		public string codigo { get; set; }
		public int? travar_protocolos { get; set; }
		public int? max_digito_ncf { get; set; }
		public long? campanha_vacinacao_id { get; set; }
		public int? sisbov_trava_sequencial { get; set; }
		public int? aparte_categoria { get; set; }
		public int? aparte_dias_gestacao { get; set; }
		public int? ignorar_inicio_zero { get; set; }
		public int? verificar_todas_fazendas { get; set; }
		public string faixa_seguranca_sisbov_inicio { get; set; }
		public string faixa_seguranca_sisbov_final { get; set; }
		public int? travar_piquete { get; set; }
		public int? reprocessamento { get; set; }
		public int? aparte_estado_fisiologico { get; set; }
		public int? bloquear_rebanho { get; set; }

		
	}
}
