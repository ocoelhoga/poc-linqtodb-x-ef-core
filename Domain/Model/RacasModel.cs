﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Model
{
    public class RacasModel
    {
		public virtual long? id { get; set; }
		public double? dt_update { get; set; }
		public double? dt_register { get; set; }
		public int? status { get; set; }
		public long? implantations_id { get; set; }
		public long? users_id { get; set; }
		public long? workgroups_id { get; set; }
		public long? last_update_user_id { get; set; }
		public string sent { get; set; }
		public string nome { get; set; }
		public string codigo { get; set; }
		public string bos_taurus { get; set; }
		public string bos_indicus { get; set; }
		public string po { get; set; }
		public long? racas_bnd_id { get; set; }
		public int? era_maxima_bezerro_bezerra { get; set; }
		public int? era_maxima_garrote { get; set; }
		public decimal? peso_minimo_bezerros_bezerras { get; set; }
		public decimal? peso_maximo_bezerros_bezerras { get; set; }
		public decimal? peso_minimo_garrote { get; set; }
		public decimal? peso_maximo_garrote { get; set; }
		public decimal? peso_minimo_novilha { get; set; }
		public decimal? peso_maximo_novilha { get; set; }
		public decimal? peso_minimo_boi_touro { get; set; }
		public decimal? peso_maximo_boi_touro { get; set; }
		public decimal? peso_minimo_vacas { get; set; }
		public decimal? peso_maximo_vacas { get; set; }
		public int? era_media_para_inicio_vacas { get; set; }
		public int? dias_minimo_nutricao_bezerro { get; set; }
		public int? dias_minimo_nutricao_bezerro_creep_feed { get; set; }
		public int? flag_disponivel { get; set; }
	}
}
