﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Model
{
	public class PiqueteCurralModel
	{
		public virtual long? id { get; set; }
		public double? dt_update { get; set; }
		public double? dt_register { get; set; }
		public int? status { get; set; }
		public long? implantations_id { get; set; }
		public long? users_id { get; set; }
		public long? workgroups_id { get; set; }
		public long? last_update_user_id { get; set; }
		public string sent { get; set; }
		public long? pasto_setor_id { get; set; }
		public double? latitude_fim { get; set; }
		public double? longitude_fim { get; set; }
		public string linha { get; set; }
		public int? numero { get; set; }
		public string tag_entrada { get; set; }
		public string tag_saida { get; set; }
		public string tag_obrigatoria { get; set; }
		public double? data_entrada { get; set; }
		public int? capacidade_animal { get; set; }
		public string nome { get; set; }
		public string codigo { get; set; }
		public double? latitude { get; set; }
		public double? longitude { get; set; }
		public decimal? area { get; set; }
		public int? tipo { get; set; }
		public long? propriedade_id { get; set; }

	}
}
