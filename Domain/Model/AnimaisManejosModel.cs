﻿using LinqToDB.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Model
{
    public class AnimaisManejosModel
    {
        public virtual long? id { get; set; }
        public double? dt_update { get; set; }
        public double? dt_register { get; set; }
        public int? status { get; set; }
        public long? implantations_id { get; set; }
        public long? users_id { get; set; }
        public long? workgroups_id { get; set; }
        public long? last_update_user_id { get; set; }
        public string sent { get; set; }
        public long? animais_id { get; set; }
        public decimal? peso { get; set; }
        public double? dt_pesagem { get; set; }
        public int? era { get; set; }
        public long? manejo_id { get; set; }
        public long? curral_manejo_id { get; set; }
        public int? score_corporal { get; set; }
        public long? rebanho_id { get; set; }
        public int? categoria_animal { get; set; }
        public string sisbov { get; set; }
        public string ncf { get; set; }
        public string chip { get; set; }
        public long? lote_manejo_destino_id { get; set; }
        public long? piquete_curral_destino_id { get; set; }
        public long? raca_id { get; set; }
        public long? propriedade_id { get; set; }
        public long? manejo_destino_id { get; set; }
        public long? protocolo_sanitario_id { get; set; }
        public long? protocolo_sanitario_add_id { get; set; }
        public decimal? gmd { get; set; }
        public long? piquete_curral_origem_id { get; set; }
        public double? dt_ultima_pesagem { get; set; }
        public decimal? ultima_pesagem { get; set; }
        public int? diferenca_dias { get; set; }
        public long? gta_id { get; set; }
        public long? documento_destino_id { get; set; }
        public int? descarte { get; set; }

    }
}
