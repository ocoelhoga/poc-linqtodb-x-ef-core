﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Model
{
    public class UsersModel
    {
        public virtual long? id { get; set; }
        public double? dt_update { get; set; }
        public double? dt_register { get; set; }
        public int? status { get; set; }
        public long? implantations_id { get; set; }
        public long? users_id { get; set; }
        public long? workgroups_id { get; set; }
        public long? last_update_user_id { get; set; }
        public string sent { get; set; }
        public string login { get; set; }
        public string password { get; set; }
        public string name { get; set; }
        public long? groups_id { get; set; }
        public string access_key { get; set; }
        public double? last_access { get; set; }
        public long? pessoas_id { get; set; }
        public string checksum { get; set; }

    }
}
