﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Model
{
    public class AnimaisModel
    {
		public virtual long? id { get; set; }
		public double? dt_update { get; set; }
		public double? dt_register { get; set; }
		public int? status { get; set; }
		public long? implantations_id { get; set; }
		public long? users_id { get; set; }
		public long? workgroups_id { get; set; }
		public long? last_update_user_id { get; set; }
		public string sent { get; set; }
		public double? dt_liberacao { get; set; }
		public string tgr_sexo { get; set; }
		public string tgr_categoria_animal { get; set; }
		public double? dt_limite_brincagem_sisbov { get; set; }
		public int? tgc { get; set; }
		public int? flag_animal_falso { get; set; }
		public string ncf { get; set; }
		public string sisbov { get; set; }
		public string chip { get; set; }
		public string tatuagem { get; set; }
		public string rgd { get; set; }
		public string rgn { get; set; }
		public double? dt_nascimento { get; set; }
		public double? dt_cadastro { get; set; }
		public int? categoria_animal { get; set; }
		public int? flag { get; set; }
		public int? ano { get; set; }
		public long? racas_id { get; set; }
		public long? rebanho_id { get; set; }
		public long? propriedade_id { get; set; }
		public long? localizacao_origem_id { get; set; }
		public long? localizacao_atual_id { get; set; }
		public long? cocho_id { get; set; }
		public double? dt_operacao { get; set; }
		public long? documento_id { get; set; }
		public long? documento_saida_id { get; set; }
		public long? gta_id { get; set; }
		public long? nota_fiscal_id { get; set; }
		public long? gta_saida_id { get; set; }
		public long? nota_fiscal_saida_id { get; set; }
		public long? contrato_id { get; set; }
		public long? lote_manejo_id { get; set; }
		public double? dt_movimentacao { get; set; }
		public DateTime dt_identificacao { get; set; }
		public int? era_entrada { get; set; }
		public long? previsao_saida_id { get; set; }
		public double? dt_entrada { get; set; }
		public int? orfao { get; set; }
	}
}
