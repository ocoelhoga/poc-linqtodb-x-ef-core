﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Model
{
	public class RebanhoModel
	{
		public virtual long? id { get; set; }
		public double? dt_update { get; set; }
		public double? dt_register { get; set; }
		public int? status { get; set; }
		public long? implantations_id { get; set; }
		public long? users_id { get; set; }
		public long? workgroups_id { get; set; }
		public long? last_update_user_id { get; set; }
		public string sent { get; set; }
		public string nome { get; set; }
		public string codigo { get; set; }
		public long? propriedade_id { get; set; }


	}
}
