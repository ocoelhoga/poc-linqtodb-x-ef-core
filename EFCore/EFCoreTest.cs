﻿using EFCore.DB;
using EFCore.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFCore
{
    public class EFCoreTest
    {
        private Context _db;
        private string _sqlSimples;
        private string _sqlComplexo;
        private Stopwatch _watch;
        private double? dtPesagem;

        public EFCoreTest()
        {
            _db = new Context();
            _watch = new Stopwatch();
            _sqlSimples = @"select am.*
                            from animais_manejos am 
                            left join animais a on a.id = am.animais_id limit {0};";

            _sqlComplexo = @"select am.*
                                from animais_manejos am 
                                left join animais a on a.id = am.animais_id and a.implantations_id = am.implantations_id 
                                left join piquete_curral pc on pc.id = am.piquete_curral_destino_id and pc.implantations_id = am.implantations_id
                                left join racas r on r.id = am.raca_id and r.implantations_id = am.implantations_id 
                                left join protocolo_sanitario ps on ps.id = am.protocolo_sanitario_id and ps.implantations_id = am.implantations_id 
                                left join manejos m on m.id = am.manejo_id and m.implantations_id = am.implantations_id 
                                left join rebanho rb on rb.id = am.rebanho_id and rb.implantations_id = am.implantations_id 
                                left join users u on u.id = am.users_id and u.implantations_id = am.implantations_id 
                                where am.propriedade_id is not null and r.nome is not null or am.era > 0
                        limit {0}";
            var asa = _db.AnimaisManejos.FromSqlRaw(_sqlComplexo, 18).ToList();

            if (_db.Database.ExecuteSqlRaw("select 1;") == -1)
                Console.WriteLine("Conexão com o banco OK!");
        }

        public void ConsultaSimples100()
        {
            _watch.Restart();
            _watch.Start();
            dynamic list = (
                         from am in _db.AnimaisManejos
                         from a in _db.Animais.Where(x => x.id == am.animais_id).DefaultIfEmpty()
                         select am
                    ).Take(100).ToList();
            _watch.Stop();
            Console.WriteLine($"Tempo consulta simples top 100       => {_watch.Elapsed}");
        }

        public void ConsultaSimples1000()
        {
            _watch.Restart();
            _watch.Start();
            dynamic list = (
                        from am in _db.AnimaisManejos
                        from a in _db.Animais.Where(x => x.id == am.animais_id).DefaultIfEmpty()
                        select am
                    ).Take(1000).ToList();
            _watch.Stop();
            Console.WriteLine($"Tempo consulta simples top 1.000     => {_watch.Elapsed}");
        }

        public void ConsultaSimples5000()
        {
            _watch.Restart();
            _watch.Start();
            dynamic list = (
                        from am in _db.AnimaisManejos
                        from a in _db.Animais.Where(x => x.id == am.animais_id).DefaultIfEmpty()
                        select am
                    ).Take(5000).ToList();
            _watch.Stop();
            Console.WriteLine($"Tempo consulta simples top 5.000     => {_watch.Elapsed}");
        }

        public void ConsultaSimples500000()
        {
            _watch.Restart();
            _watch.Start();
            dynamic list = (
                        from am in _db.AnimaisManejos
                        from a in _db.Animais.Where(x => x.id == am.animais_id).DefaultIfEmpty()
                        select am
                    ).Take(500000).ToList();
            _watch.Stop();
            Console.WriteLine($"Tempo consulta simples top 500.000   => {_watch.Elapsed}");
        }

        public void ConsultaSimples1000000()
        {
            _watch.Restart();
            _watch.Start();
            dynamic list = (
                        from am in _db.AnimaisManejos
                        from a in _db.Animais.Where(x => x.id == am.animais_id).DefaultIfEmpty()
                        select am
                    ).Take(1000000).ToList();
            _watch.Stop();
            Console.WriteLine($"Tempo consulta simples top 1.000.000 => {_watch.Elapsed}");
        }

        public void ConsultaSimplesRaw100()
        {
            _watch.Restart();
            _watch.Start();
            _db.AnimaisManejos.FromSqlRaw(_sqlSimples, 100).ToList();
            _watch.Stop();
            Console.WriteLine($"Tempo consulta simples raw top 100       => {_watch.Elapsed}");
        }

        public void ConsultaSimplesRaw1000()
        {
            _watch.Restart();
            _watch.Start(); _db.AnimaisManejos.FromSqlRaw(_sqlSimples, 1000).ToList();
            _watch.Stop();
            Console.WriteLine($"Tempo consulta simples raw top 1.000     => {_watch.Elapsed}");
        }

        public void ConsultaSimplesRaw5000()
        {
            _watch.Restart();
            _watch.Start(); _db.AnimaisManejos.FromSqlRaw(_sqlSimples, 5000).ToList();
            _watch.Stop();
            Console.WriteLine($"Tempo consulta simples raw top 5.000     => {_watch.Elapsed}");
        }

        public void ConsultaSimplesRaw500000()
        {
            _watch.Restart();
            _watch.Start(); _db.AnimaisManejos.FromSqlRaw(_sqlSimples, 500000).ToList();
            _watch.Stop();
            Console.WriteLine($"Tempo consulta simples raw top 500.000   => {_watch.Elapsed}");
        }

        public void ConsultaSimplesRaw1000000()
        {
            _watch.Restart();
            _watch.Start(); _db.AnimaisManejos.FromSqlRaw(_sqlSimples, 1000000).ToList();
            _watch.Stop();
            Console.WriteLine($"Tempo consulta simples raw top 1.000.000 => {_watch.Elapsed}");
        }

        public void ConsultaComplexa100()
        {
            _watch.Restart();
            _watch.Start();
            dynamic list = (
             from am in _db.AnimaisManejos
             from a in _db.Animais.Where(x => x.id == am.animais_id).DefaultIfEmpty()
             from pc in _db.PiqueteCurral.Where(x => x.id == am.piquete_curral_destino_id).DefaultIfEmpty()
             from r in _db.Racas.Where(x => x.id == am.raca_id).DefaultIfEmpty()
             from ps in _db.ProtocoloSanitario.Where(x => x.id == am.protocolo_sanitario_id).DefaultIfEmpty()
             from m in _db.Manejos.Where(x => x.id == am.manejo_id).DefaultIfEmpty()
             from rb in _db.Rebanho.Where(x => x.id == am.rebanho_id).DefaultIfEmpty()
             from u in _db.Users.Where(x => x.id == am.users_id).DefaultIfEmpty()
             select am
            ).Take(100).ToList();
            _watch.Stop();
            Console.WriteLine($"Tempo consulta complexa top 100       => {_watch.Elapsed}");
        }

        public void ConsultaComplexa1000()
        {
            _watch.Restart();
            _watch.Start();
            dynamic list = (
             from am in _db.AnimaisManejos
             from a in _db.Animais.Where(x => x.id == am.animais_id).DefaultIfEmpty()
             from pc in _db.PiqueteCurral.Where(x => x.id == am.piquete_curral_destino_id).DefaultIfEmpty()
             from r in _db.Racas.Where(x => x.id == am.raca_id).DefaultIfEmpty()
             from ps in _db.ProtocoloSanitario.Where(x => x.id == am.protocolo_sanitario_id).DefaultIfEmpty()
             from m in _db.Manejos.Where(x => x.id == am.manejo_id).DefaultIfEmpty()
             from rb in _db.Rebanho.Where(x => x.id == am.rebanho_id).DefaultIfEmpty()
             from u in _db.Users.Where(x => x.id == am.users_id).DefaultIfEmpty()
             select am
            ).Take(1000).ToList();
            _watch.Stop();
            Console.WriteLine($"Tempo consulta complexa top 1000      => {_watch.Elapsed}");
        }

        public void ConsultaComplexa5000()
        {
            _watch.Restart();
            _watch.Start();
            dynamic list = (
             from am in _db.AnimaisManejos
             from a in _db.Animais.Where(x => x.id == am.animais_id).DefaultIfEmpty()
             from pc in _db.PiqueteCurral.Where(x => x.id == am.piquete_curral_destino_id).DefaultIfEmpty()
             from r in _db.Racas.Where(x => x.id == am.raca_id).DefaultIfEmpty()
             from ps in _db.ProtocoloSanitario.Where(x => x.id == am.protocolo_sanitario_id).DefaultIfEmpty()
             from m in _db.Manejos.Where(x => x.id == am.manejo_id).DefaultIfEmpty()
             from rb in _db.Rebanho.Where(x => x.id == am.rebanho_id).DefaultIfEmpty()
             from u in _db.Users.Where(x => x.id == am.users_id).DefaultIfEmpty()
             select am
            ).Take(5000).ToList();
            _watch.Stop();
            Console.WriteLine($"Tempo consulta complexa top 5.000     => {_watch.Elapsed}");
        }

        public void ConsultaComplexa500000()
        {
            _watch.Restart();
            _watch.Start();
            dynamic list = (
             from am in _db.AnimaisManejos
             from a in _db.Animais.Where(x => x.id == am.animais_id).DefaultIfEmpty()
             from pc in _db.PiqueteCurral.Where(x => x.id == am.piquete_curral_destino_id).DefaultIfEmpty()
             from r in _db.Racas.Where(x => x.id == am.raca_id).DefaultIfEmpty()
             from ps in _db.ProtocoloSanitario.Where(x => x.id == am.protocolo_sanitario_id).DefaultIfEmpty()
             from m in _db.Manejos.Where(x => x.id == am.manejo_id).DefaultIfEmpty()
             from rb in _db.Rebanho.Where(x => x.id == am.rebanho_id).DefaultIfEmpty()
             from u in _db.Users.Where(x => x.id == am.users_id).DefaultIfEmpty()
             select am
            ).Take(500000).ToList();
            _watch.Stop();
            Console.WriteLine($"Tempo consulta complexa top 500.000   => {_watch.Elapsed}");
        }

        public void ConsultaComplexa1000000()
        {
            _watch.Restart();
            _watch.Start();
            dynamic list = (
             from am in _db.AnimaisManejos
             from a in _db.Animais.Where(x => x.id == am.animais_id).DefaultIfEmpty()
             from pc in _db.PiqueteCurral.Where(x => x.id == am.piquete_curral_destino_id).DefaultIfEmpty()
             from r in _db.Racas.Where(x => x.id == am.raca_id).DefaultIfEmpty()
             from ps in _db.ProtocoloSanitario.Where(x => x.id == am.protocolo_sanitario_id).DefaultIfEmpty()
             from m in _db.Manejos.Where(x => x.id == am.manejo_id).DefaultIfEmpty()
             from rb in _db.Rebanho.Where(x => x.id == am.rebanho_id).DefaultIfEmpty()
             from u in _db.Users.Where(x => x.id == am.users_id).DefaultIfEmpty()
             select am
            ).Take(1000000).ToList();
            _watch.Stop();
            Console.WriteLine($"Tempo consulta complexa top 1.000.000 => {_watch.Elapsed}");
        }

        public void ConsultaComplexaRaw100()
        {
            _watch.Restart();
            _watch.Start();
            _db.AnimaisManejos.FromSqlRaw(_sqlComplexo, 100).ToList();
            _watch.Stop();
            Console.WriteLine($"Tempo consulta complexa raw top 100       => {_watch.Elapsed}");
        }

        public void ConsultaComplexaRaw1000()
        {
            _watch.Restart();
            _watch.Start();
            _db.AnimaisManejos.FromSqlRaw(_sqlComplexo, 1000).ToList();
            _watch.Stop();
            Console.WriteLine($"Tempo consulta complexa raw top 1.000     => {_watch.Elapsed}");
        }

        public void ConsultaComplexaRaw5000()
        {
            _watch.Restart();
            _watch.Start();
            _db.AnimaisManejos.FromSqlRaw(_sqlComplexo, 5000).ToList();
            _watch.Stop();
            Console.WriteLine($"Tempo consulta complexa raw top 5.000     => {_watch.Elapsed}");
        }

        public void ConsultaComplexaRaw500000()
        {
            _watch.Restart();
            _watch.Start();
            _db.AnimaisManejos.FromSqlRaw(_sqlComplexo, 500000).ToList();
            _watch.Stop();
            Console.WriteLine($"Tempo consulta complexa raw top 500.000   => {_watch.Elapsed}");
        }

        public void ConsultaComplexaRaw1000000()
        {
            _watch.Restart();
            _watch.Start();
            _db.AnimaisManejos.FromSqlRaw(_sqlComplexo, 1000000).ToList();
            _watch.Stop();
            Console.WriteLine($"Tempo consulta complexa raw top 1.000.000 => {_watch.Elapsed}");
        }

        public void Insert100()
        {
            int count = 0;
            List<animais_manejos> list = new List<animais_manejos>();
            _watch.Restart();
            while (count < 100)
            {
                animais_manejos am = new animais_manejos()
                {
                    status = 1,
                    implantations_id = 4,
                    users_id = 1,
                    sent = "N",
                    animais_id = 52900000258,
                    peso = 489756,
                    dt_pesagem = dtPesagem.HasValue ? dtPesagem + 1001 : 1635429069,
                    era = 4,
                    manejo_id = 58300000001,
                    score_corporal = 2,
                    rebanho_id = 100000001,
                    categoria_animal = 3,
                    ncf = "3420264",
                    piquete_curral_destino_id = 41800000001,
                    raca_id = 25,
                    propriedade_id = 100000001,
                    protocolo_sanitario_id = 1600000001
                };
                dtPesagem = am.dt_pesagem;
                list.Add(am);
                count++;
            }
            _watch.Start();
            _db.AnimaisManejos.AddRange(list);
            _db.SaveChanges();
            _watch.Stop();
            Console.WriteLine($"Tempo insert 100  => {_watch.Elapsed}");
        }

        public void Insert500()
        {
            int count = 0;
            List<animais_manejos> list = new List<animais_manejos>();
            _watch.Restart();
            while (count < 500)
            {
                animais_manejos am = new animais_manejos()
                {
                    status = 1,
                    implantations_id = 4,
                    users_id = 1,
                    sent = "N",
                    animais_id = 52900000258,
                    peso = 489756,
                    dt_pesagem = dtPesagem.HasValue ? dtPesagem + 1001 : 1635429069,
                    era = 4,
                    manejo_id = 58300000001,
                    score_corporal = 2,
                    rebanho_id = 100000001,
                    categoria_animal = 3,
                    ncf = "3420264",
                    piquete_curral_destino_id = 41800000001,
                    raca_id = 25,
                    propriedade_id = 100000001,
                    protocolo_sanitario_id = 1600000001
                };
                dtPesagem = am.dt_pesagem;
                list.Add(am);
                count++;
            }
            _watch.Start();
            _db.AnimaisManejos.AddRange(list);
            _db.SaveChanges();
            _watch.Stop();
            Console.WriteLine($"Tempo insert 500  => {_watch.Elapsed}");
        }

        public void Insert1000()
        {
            int count = 0;
            List<animais_manejos> list = new List<animais_manejos>();
            _watch.Restart();
            while (count < 1000)
            {
                animais_manejos am = new animais_manejos()
                {
                    status = 1,
                    implantations_id = 4,
                    users_id = 1,
                    sent = "N",
                    animais_id = 52900000258,
                    peso = 489756,
                    dt_pesagem = dtPesagem.HasValue ? dtPesagem + 1001 : 1635429069,
                    era = 4,
                    manejo_id = 58300000001,
                    score_corporal = 2,
                    rebanho_id = 100000001,
                    categoria_animal = 3,
                    ncf = "3420264",
                    piquete_curral_destino_id = 41800000001,
                    raca_id = 25,
                    propriedade_id = 100000001,
                    protocolo_sanitario_id = 1600000001
                };
                dtPesagem = am.dt_pesagem;
                list.Add(am);
                count++;
            }
            _watch.Start();
            _db.AnimaisManejos.AddRange(list);
            _db.SaveChanges();
            _watch.Stop();
            Console.WriteLine($"Tempo insert 1000 => {_watch.Elapsed}");
            DeleteInserts();
        }

        public void DeleteInserts()
        {
            _db.AnimaisManejos.RemoveRange(_db.AnimaisManejos.Where(x => x.peso == 489756));
            _db.SaveChanges();
        }
    }
}
