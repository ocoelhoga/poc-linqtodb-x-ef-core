﻿using System;

namespace EFCore
{
    class Program
    {
        static void Main(string[] args)
        {
            EFCoreTest teste = new EFCoreTest();
            Console.WriteLine("===============================================================");
            teste.ConsultaSimples100();
            teste.ConsultaSimples1000();
            teste.ConsultaSimples5000();
            teste.ConsultaSimples500000();
            teste.ConsultaSimples1000000();
            Console.WriteLine("===============================================================");
            Console.WriteLine("===============================================================");
            teste.ConsultaSimplesRaw100();
            teste.ConsultaSimplesRaw1000();
            teste.ConsultaSimplesRaw5000();
            teste.ConsultaSimplesRaw500000();
            teste.ConsultaSimplesRaw1000000();
            Console.WriteLine("===============================================================");

            Console.WriteLine("===============================================================");
            teste.ConsultaComplexa100();
            teste.ConsultaComplexa1000();
            teste.ConsultaComplexa5000();
            teste.ConsultaComplexa500000();
            teste.ConsultaComplexa1000000();
            Console.WriteLine("===============================================================");
            Console.WriteLine("===============================================================");
            teste.ConsultaComplexaRaw100();
            teste.ConsultaComplexaRaw1000();
            teste.ConsultaComplexaRaw5000();
            teste.ConsultaComplexaRaw500000();
            teste.ConsultaComplexaRaw1000000();
            Console.WriteLine("===============================================================");
            Console.WriteLine("===============================================================");
            teste.Insert100();
            teste.Insert500();
            teste.Insert1000();
            Console.WriteLine("===============================================================");
        }
    }
}
