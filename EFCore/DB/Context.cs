﻿using EFCore.Model;
using Microsoft.EntityFrameworkCore;
using System.Reflection;

namespace EFCore.DB
{
    public class Context : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
            => optionsBuilder.UseNpgsql(@"User ID=postgres;Password=postgres;Server=127.0.0.1;Port=5432;Database=piratininga;Integrated Security=true;Pooling=true;");

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<manejos>().ToTable("manejos").HasKey(x => x.id);
            modelBuilder.Entity<piquete_curral>().ToTable("piquete_curral").HasKey(x => x.id);
            modelBuilder.Entity<protocolo_sanitario>().ToTable("protocolo_sanitario").HasKey(x => x.id);
            modelBuilder.Entity<racas>().ToTable("racas").HasKey(x => x.id);
            modelBuilder.Entity<rebanho>().ToTable("rebanho").HasKey(x => x.id);
            modelBuilder.Entity<users>().ToTable("users").HasKey(x => x.id);

            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());

        }

        public DbSet<animais> Animais { get; set; }
        public DbSet<animais_manejos> AnimaisManejos { get; set; }
        public DbSet<manejos> Manejos { get; set; }
        public DbSet<piquete_curral> PiqueteCurral { get; set; }
        public DbSet<protocolo_sanitario> ProtocoloSanitario { get; set; }
        public DbSet<racas> Racas { get; set; }
        public DbSet<rebanho> Rebanho { get; set; }
        public DbSet<users> Users { get; set; }
        public DbSet<RetornoSqlSimples> RetornoSqlSimples { get; set; }
        public DbSet<RetornoSqlComplexo> RetornoSqlComplexo { get; set; }

    }
}
