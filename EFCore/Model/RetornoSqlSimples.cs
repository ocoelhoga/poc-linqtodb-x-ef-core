﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFCore.Model
{
    public class RetornoSqlSimples
    {
        public long? id { get; set; }
        public string sisbov { get; set; }
        public string chip{ get; set; }
        public string ncf{ get; set; }
    }
}
