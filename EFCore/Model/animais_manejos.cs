﻿using Domain.Model;
using System;

namespace EFCore.Model
{
    public class animais_manejos : AnimaisManejosModel
    {
        public animais Animais { get; set; }
        public static animais_manejos Novo()
        {
            return new animais_manejos()
            {
                status = 1,
                implantations_id = 4,
                users_id = 1,
                sent = "N",
                animais_id = 52900000258,
                peso = 489756,
                dt_pesagem = 1635429069,
                era = 4,
                manejo_id = 58300000001,
                score_corporal = 2,
                rebanho_id = 100000001,
                categoria_animal = 3,
                ncf = "3420264",
                piquete_curral_destino_id = 41800000001,
                raca_id = 25,
                propriedade_id = 100000001,
                protocolo_sanitario_id = 1600000001
            };
        }
    }
}
