﻿using LinqToDB.Data;
using LinqToDB.Mapping;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFCore.Model.Configuration
{
    public class ConfigurationAnimaisManejos : IEntityTypeConfiguration<animais_manejos>
    {
        public void Configure(EntityTypeBuilder<animais_manejos> builder)
        {
            builder.ToTable("animais_manejos");
            
            builder.HasKey(x => x.id);
           
            builder.HasOne<animais>(x => x.Animais)
               .WithMany()
               .HasForeignKey(x => x.animais_id);

            builder.HasOne<piquete_curral>()
               .WithMany()
               .HasForeignKey(x => x.piquete_curral_destino_id );

            builder.Property(x => x.animais_id)
                .IsRequired();
        }
    }
}
