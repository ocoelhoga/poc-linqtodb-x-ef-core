﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EFCore.Model.Configuration
{
    public class ConfigurationAnimais : IEntityTypeConfiguration<animais>
    {
        public void Configure(EntityTypeBuilder<animais> builder)
        {
            builder.ToTable("animais");

            builder.HasKey(x => x.id);
        }
    }
}
