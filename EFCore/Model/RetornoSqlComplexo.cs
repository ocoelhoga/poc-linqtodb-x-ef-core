﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFCore.Model
{
    public class RetornoSqlComplexo : RetornoSqlSimples
    {
        public string nome { get; set; }
        public string codigo { get; set; }
        public long? protocolo { get; set; }
        public double? dt_inicio { get; set; }
        public long? propriedade_id { get; set; }
        public long? pessoas_id { get; set; }
    }
}
